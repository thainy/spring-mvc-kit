package org.thainy.spring.mvc.kit.json;

import org.springframework.http.HttpHeaders;

public class JsonNoCacheHeaders extends HttpHeaders {
	private static final long serialVersionUID = 1L;

	public JsonNoCacheHeaders() {
		setExpires(0);
		setPragma("no-cache");
		setCacheControl("no-cache");
		add("Content-Type", "application/json;charset=utf-8");
	}
}