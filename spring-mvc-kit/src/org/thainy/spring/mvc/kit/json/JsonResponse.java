package org.thainy.spring.mvc.kit.json;

import java.io.Serializable;
import java.text.DecimalFormat;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.google.gson.Gson;

public class JsonResponse<T> extends ResponseEntity<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final DecimalFormat ERROR_CODE_FORMAT = new DecimalFormat("00000");

	private boolean success = true;

	private String errorCode;

	private String errorMessage;

	private String redirectTo;

	public Integer apiKeyUsedCount;

	private T data;

	public JsonResponse() {
		super(new JsonNoCacheHeaders(), HttpStatus.OK);
	}

	public JsonResponse(T data) {
		super(new JsonNoCacheHeaders(), HttpStatus.OK);
		this.data = data;
	}

	public JsonResponse(HttpStatus httpStatus) {
		super(new JsonNoCacheHeaders(), httpStatus);
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		try {
			int code = Integer.parseInt(errorCode);
			this.errorCode = ERROR_CODE_FORMAT.format(code);
		} catch (Exception e) {
			this.errorCode = errorCode;
		}
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getRedirectTo() {
		return redirectTo;
	}

	public void setRedirectTo(String redirectTo) {
		this.redirectTo = redirectTo;
	}

	public Integer getApiKeyUsedCount() {
		return apiKeyUsedCount;
	}

	public void setApiKeyUsedCount(Integer apiKeyUsedCount) {
		this.apiKeyUsedCount = apiKeyUsedCount;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		Gson gson = JsonConstant.JSON_RESP_BUILDER.create();
		return gson.toJson(this);
	}

	@Override
	public String getBody() {
		return toString();
	}

	public JsonResponse<T> fail(String errorCode, String errorMessage) {
		success = false;
		setErrorCode(errorCode);
		this.errorMessage = errorMessage;
		return this;
	}

	public JsonResponse<T> fail(String errorMessage) {
		success = false;
		setErrorCode("500");
		this.errorMessage = errorMessage;
		return this;
	}

	public JsonResponse<T> redirectTo(String redirectTo) {
		this.redirectTo = redirectTo;
		return this;
	}

}
