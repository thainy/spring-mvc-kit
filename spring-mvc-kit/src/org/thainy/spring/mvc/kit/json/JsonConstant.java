package org.thainy.spring.mvc.kit.json;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

public abstract class JsonConstant {
	static final String ISO_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	public static final GsonBuilder JSON_RESP_BUILDER;
	static {
		JSON_RESP_BUILDER = new GsonBuilder();
		JSON_RESP_BUILDER.setDateFormat(ISO_DATE_PATTERN);
		JSON_RESP_BUILDER.setLongSerializationPolicy(LongSerializationPolicy.DEFAULT);
		JSON_RESP_BUILDER.serializeNulls();
		JSON_RESP_BUILDER.addSerializationExclusionStrategy(new ExclusionStrategy() {
			@Override
			public boolean shouldSkipField(FieldAttributes f) {
				Class<?> declaringClass = f.getDeclaringClass();
				if (declaringClass.equals(ResponseEntity.class) || declaringClass.equals(HttpEntity.class)) {
					return true;
				}
				if (f.getAnnotation(SkipGson.class) != null) {
					return true;
				}
				return false;
			}

			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				return false;
			}
		});
	}

}
