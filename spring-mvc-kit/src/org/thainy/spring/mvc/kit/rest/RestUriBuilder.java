package org.thainy.spring.mvc.kit.rest;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class RestUriBuilder {
	private static final ConcurrentHashMap<Class<?>, RestUriBuilder> BUILDERS = new ConcurrentHashMap<Class<?>, RestUriBuilder>();
	private static String defaultDatePattern = "dd/MM/yyyy";
	private static Locale defaultLocale = Locale.US;
	private String apiUri;
	private List<Field> parameters;
	private List<Field> pathVariables;

	private RestUriBuilder(String apiUri, Class<?> target) {
		this.apiUri = apiUri;
		parameters = new ArrayList<Field>();
		pathVariables = new ArrayList<Field>();
		readFileds(target);
	}

	public static void setDefaultDatePatter(String pattern, Locale locale) {
		defaultDatePattern = Objects.requireNonNull(pattern);
		defaultLocale = Objects.requireNonNull(locale);
	}

	public static String build(String apiUri, Object request) throws RestClientException {
		RestUriBuilder builder = BUILDERS.computeIfAbsent(request.getClass(), k -> new RestUriBuilder(apiUri, k));
		StringBuilder requestUri = new StringBuilder();
		requestUri.append(builder.applyPathVariable(request));
		String requestParams = builder.buildParam(request);
		if (requestParams.length() > 0) {
			requestUri.append("?").append(requestParams);
		}
		return requestUri.toString();
	}

	private void readFileds(Class<?> target) {
		if (target == null) {
			return;
		}
		Field[] declaredFields = target.getDeclaredFields();
		for (Field field : declaredFields) {
			if (field.getAnnotation(RestPathVariable.class) != null) {
				field.setAccessible(true);
				pathVariables.add(field);
			} else if (!Modifier.isStatic(field.getModifiers())
					&& field.getAnnotation(RestParamExcludes.class) == null) {
				field.setAccessible(true);
				parameters.add(field);
			}
		}
		readFileds(target.getSuperclass());
	}

	private String applyPathVariable(Object request) throws RestClientException {
		String uri = apiUri;
		try {
			for (Field field : pathVariables) {
				RestPathVariable annotation = field.getAnnotation(RestPathVariable.class);
				String fieldName = annotation.value();
				if (fieldName.isBlank()) {
					fieldName = field.getName();
				}
				uri = uri.replaceAll("[{]" + fieldName + "[}]", toUriValue(field.get(request)));
			}
		} catch (Exception e) {
			throw new RestClientException(e);
		}
		return uri;
	}

	private String buildParam(Object request) throws RestClientException {
		StringBuilder params = new StringBuilder();
		try {
			for (int i = 0; i < parameters.size(); i++) {
				Field field = parameters.get(i);
				Object value = field.get(request);
				if (value != null) {
					String paramValue = "";
					paramValue = toUriValue(value);
					params.append("&").append(field.getName()).append("=")
							.append(URLEncoder.encode(paramValue, "UTF-8"));
				}
			}
			if (params.length() > 0) {
				return params.toString().substring(1);
			}
		} catch (Exception e) {
			throw new RestClientException(e);
		}
		return "";
	}

	private String toUriValue(Object value) {
		String paramValue = null;
		if (Date.class.equals(value.getClass()) || java.sql.Date.class.equals(value.getClass())) {
			paramValue = new SimpleDateFormat(defaultDatePattern, defaultLocale).format(value);
		} else if (Collection.class.isAssignableFrom(value.getClass())) {
			Collection<?> collection = (Collection<?>) value;
			if (!collection.isEmpty()) {
				paramValue = collection.stream().map(String::valueOf).collect(Collectors.joining(","));
			}
		} else {
			paramValue = String.valueOf(value);
		}
		return paramValue;
	}

}
