package org.thainy.spring.mvc.kit.rest;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Path to the API relative to the server URL.
 * 
 * @author User
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface RestEndpoint {
	String value() default "";
}
