package org.thainy.spring.mvc.kit.rest;

public class RestClientException extends Exception {
	private static final long serialVersionUID = 1L;

	public RestClientException(Exception e) {
		super(e);
	}

}
